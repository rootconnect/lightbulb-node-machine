import fetch from 'node-fetch'

import { generateToken } from '../auth/auth'

export const api = async(path, body, options={'Content-Type': 'application/json'}) => {
    const token = await generateToken()
    const baseURL = 'http://192.168.1.95:3001'

    return fetch(`${baseURL}${path}`, {
        method: options.method,
        headers: {...options.headers,'Authorization': `Bearer ${token}`},
        body
    })
    .then(function(res) {
        return res.json();
    })
} 