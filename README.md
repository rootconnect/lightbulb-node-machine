**Lightbulb Node Machine**

***AP Mode for Machine***

[Click here](https://www.raspberrypi.org/documentation/configuration/wireless/access-point-routed.md) to see tutorial. Also, use in conjunction with [this tutorial](https://github.com/XECDesign/documentation/blob/wifi-ap-update/configuration/wireless/access-point.md)


***Notes***

- wpa_supplicant.conf must be modifiable in order to connect to a different wifi SSID. this is done using the following command: sudo chmod 666 /etc/wpa_supplicant/wpa_supplicant.conf
    - run the following command to set the wifi credentials: sudo wpa_passphrase ssid password >> /etc/wpa_supplicant/wpa_supplicant.conf
    - afterward, use sudo systemctl daemon-reload + sudo systemctl restart dhcpcd for the changes to take effect 
- Redis must be installed. [Be sure to use 6.0 or later](https://redis.io/download)