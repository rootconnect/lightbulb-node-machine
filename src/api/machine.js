import { api } from './instance'
import { getUser } from '../firebase/firebase'
import { CREDENTIALS, DYNAMIC_VARS } from '../config/constants'

export const updateMachineUserId = async() => {

    const { user } = DYNAMIC_VARS

    console.log(user.uid)
        
    await api(`/machines/${CREDENTIALS.machineId}/inituser`, {
        userUd: user.uid
    } , {
        method: 'PUT'
    })

}