import fs from 'fs'
export const JWT_MAX_AGE = '1h';

export const CREDENTIALS = JSON.parse(fs.readFileSync('credentials.json'))
const DYNAMIC_VARS = {}

export const set = (key, value) => {
    DYNAMIC_VARS[key] = value
}

export const get = key => DYNAMIC_VARS[key]