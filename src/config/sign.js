 import fs from 'fs';
 import path from 'path';
 import { JWT_MAX_AGE } from './constants'

 let privateKeyPath   =  path.join(__dirname,"..","..","keys","id_rsa");
 let publicKeyPath = path.join(__dirname,"..","..","keys","id_rsa.pub");
 
 export const privateKey = fs.readFileSync(privateKeyPath,"utf-8");
 export const publicKey = fs.readFileSync(publicKeyPath,"utf-8");
 export const signOptions = {    
    issuer:  "Lightbulb",
    expiresIn:  JWT_MAX_AGE,
    algorithm:  "RS512",
}