import ffmpeg from "fluent-ffmpeg";
import util from "util"
import cp from "child_process"

const exec = util.promisify(cp.exec)

export const record = async(fileName, settings) => {

    const { stdout, stderr } = await exec(`raspivid -o /tmp/${fileName}.h264 -t ${settings.videoLength} -w 1920 -h 1080 -p 0,0,1920,1080 -fps 25`)

    console.log(stdout, stderr)

    await new Promise((resolve, reject) => {
        ffmpeg(`/tmp/${fileName}.h264`) 
        .outputOptions("-c:v", "copy")
        .on('progress', (progress) => {
            console.log(`[ffmpeg] ${JSON.stringify(progress)}`);
        })
        .on('error', (err) => {
            console.log(`[ffmpeg] error: ${err.message}`);
            reject(err);
        })
        .on('end', () => {
            console.log('[ffmpeg] finished');
            resolve();
        })
        .save(`/tmp/${fileName}.mp4`)
    })

}