import { api } from './instance'
import path from 'path'
import fs from 'fs'
import FormData from 'form-data'

import { CREDENTIALS } from '../config/constants'

export const uploadVideo = async(videoPath = "") => {
    const newFile = fs.createReadStream(videoPath !== "" ? videoPath : path.join(__dirname, "..", "..", "test.mp4"))

    const form = new FormData();
    form.append('video', newFile);

    console.log("here", CREDENTIALS)
        
    await api(`/machines/${CREDENTIALS.machineId}/videos`, form, {
        method: 'POST',
        headers: form.getHeaders()
    })

}