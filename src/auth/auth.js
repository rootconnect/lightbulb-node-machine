import jwt from 'jsonwebtoken'

import { privateKey, signOptions } from '../config/sign'

export const generateToken = async() => {

    const { machineId, code } = process.env
    
    const sig = {
        sub: code,
        aud: machineId
    }
    
    const token = await jwt.sign(sig, privateKey, signOptions)

    return token
}