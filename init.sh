ssh-keygen -t rsa -b 4096 -q -N "" -m PEM -f $HOME/Documents/dev/lightbulb-node-machine/keys/id_rsa
openssl rsa -in $HOME/Documents/dev/lightbulb-node-machine/keys/id_rsa -pubout -outform PEM -out $HOME/Documents/dev/lightbulb-node-machine/keys/id_rsa.pub

curl -F 'key=@keys/id_rsa.pub' -o credentials.json -X POST http://localhost:3001/machines